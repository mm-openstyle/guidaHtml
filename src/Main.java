import it.openstyle.domain.Corso;
import it.openstyle.util.HtmlGeneratorUtil;
import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.nodes.Document;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        // applicare come parametro "-Durl=http://www.html.it/guide/ionic-2-la-guida" in VM options
        //http://www.html.it/guide/guida-jsp/
        String urlProperty = System.getProperty("url");
        if (urlProperty != null) {
            String[] schemes = {"http","https"};
            UrlValidator urlValidator = new UrlValidator(schemes);
            if (urlValidator.isValid(urlProperty)) {
                HtmlGeneratorUtil htmlGeneratorUtil = new HtmlGeneratorUtil();
                Document document = htmlGeneratorUtil.getHtmlDocument(urlProperty);
                Corso corso = htmlGeneratorUtil.getCorso(document);
                System.out.println(corso.toString());
            }
            else
                System.out.println("Indirizzo non valido");
        }
        else
            System.out.println("Nessun indirizzo presente");

    }






}
