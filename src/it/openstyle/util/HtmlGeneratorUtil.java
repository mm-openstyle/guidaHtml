package it.openstyle.util;

import it.openstyle.domain.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.io.File;
import java.net.URL;
import java.util.Iterator;

/**
 * Created by Manuel on 07/04/2017.
 */
public class HtmlGeneratorUtil {

    private String PATH;
    private String PATH_IMG;
    private String PATH_ATTACH;
    private String PATH_CSS;

    public HtmlGeneratorUtil() {
        PATH = System.getProperty("user.dir") + "/output";
    }

    public void initPath(String title) {
        PATH += "/" + title;
        PATH_IMG = PATH + "/img";
        PATH_ATTACH = PATH + "/attach";
        PATH_CSS = PATH + "/css";
    }

    public Document getHtmlDocument(String url) throws IOException {

        URL oracle = new URL(url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(oracle.openStream()));

        String inputLine;
        String captureHtml = "";
        while ((inputLine = in.readLine()) != null)
            captureHtml += inputLine;
        in.close();

        return Jsoup.parse(captureHtml);
    }

    public Corso getCorso(Document document) throws IOException, InterruptedException {

        String title = document.select("div[class='article-header-item']").select("h1").text();
        initPath(title);

        Corso corso = new Corso(PATH, title, null);

        //lista di capitoli
        Elements elements = document.select("div[class='entry guide-item']")
                .select("ol").select("a");
        Iterator<Element> i = elements.iterator();
        int chapter = 1;
        while (i.hasNext()) {
            Element el = i.next();
            System.out.println(el.attr("href") + " - " + el.text());
            Document subDocument = getHtmlDocument(el.attr("href"));
            Element titleEl = subDocument.select("div[class='article-header-item'").select("h1").first();

            Capitolo capitolo = new Capitolo(PATH, chapter + " - " + el.text(), null);

            //elementi da rimuovere
            Elements subElements = subDocument.select("div[class='entry guide-item content-text']");
            Element elToRemove = subElements.select("p:contains(Link utili)").first();
            if (elToRemove == null)
                elToRemove = subElements.select("div[class='row responsive-grid example-1']").first();
            Elements elsToRemove = elToRemove.parents().first().children();
            int index = elsToRemove.indexOf(elToRemove);
            while (index < elsToRemove.size()) {
                elsToRemove.get(index).remove();
                index++;
            }

            //html in stringa
            String outerHtml = titleEl.outerHtml() + "\n" + subElements.first().outerHtml();

            //controllo immagini
            Elements imgEls = subElements.select("img");
            Iterator<Element> iterator = imgEls.iterator();
            while (iterator.hasNext()) {
                Element imgEl = iterator.next();
                String filename = imgEl.attr("src").substring(imgEl.attr("src").lastIndexOf("/"), imgEl.attr("src").length());
                FileCorso immagine = new Immagine(PATH_IMG, filename, imgEl.attr("src"));
                immagine.create();
                outerHtml = outerHtml.replace(imgEl.attr("src"), immagine.getAbsolutePath().substring(immagine.getAbsolutePath().lastIndexOf("img"), immagine.getAbsolutePath().length())).replace("\\", "/");
                capitolo.addImmagine((Immagine) immagine);
            }

            //controllo allegati
            Elements attachEls = subDocument.select("div[class='attach-item']");
            Iterator<Element> attachIterator = attachEls.iterator();
            while (attachIterator.hasNext()) {
                Element attachEl = attachIterator.next();
                String filename = attachEl.select("a").attr("href");
                filename = filename.substring(filename.lastIndexOf("/"), filename.length());
                FileCorso allegato = new Allegato(PATH_ATTACH, filename, attachEl.select("a").attr("href"));
                allegato.create();
                outerHtml = outerHtml.replace(attachEl.select("a").attr("href"), allegato.getAbsolutePath().substring(allegato.getAbsolutePath().lastIndexOf("attach"), allegato.getAbsolutePath().length())).replace("\\", "/");
                outerHtml = outerHtml.replace(attachEl.select("a").text(), "Visualizza allegato");
                capitolo.addAllegato((Allegato) allegato);
            }

            //salvataggio su file su disco
            capitolo.setOuterHtml(outerHtml);
            saveHtmlFile(capitolo);
            corso.addCapitolo(capitolo);


            //System.out.println(outerHtml);
            System.out.println("...avanzo...");
            Thread.sleep(500);
            chapter++;
        }

        //aggiunta css
        String contentCss = "* {\n" +
                "    font-family: Arial, Sans-serif;\n" +
                "    font-size: 10px;\n" +
                "    font-color:#333;\n" +
                "    font-weight: normal;\n" +
                "}\n" +
                "\n" +
                "h1,h2,h3,h4,h5,h6 {\n" +
                "    font-weight: bold;\n" +
                "    font-size: 2em;\n" +
                "}\n" +
                "\n" +
                "b,strong {font-weight: bold;}\n" +
                "\n" +
                "div, p, ul, li, tr, td, th, span, b, strong {\n" +
                "    font-size: 14px;\n" +
                "}\n" +
                "\n" +
                "pre {\n" +
                "    white-space: pre-wrap;                 /* CSS3 browsers  */\n" +
                "    white-space: -moz-pre-wrap !important; /* 1999+ Mozilla  */\n" +
                "    white-space: -pre-wrap;                /* Opera 4 thru 6 */\n" +
                "    white-space: -o-pre-wrap;              /* Opera 7 and up */\n" +
                "    word-wrap: break-word;                 /* IE 5.5+ and up */\n" +
                "    font-family: monospace;\n" +
                "    display: block;\n" +
                "    border: 1px solid #444;\n" +
                "    background: #DADADA; \n" +
                "    width: 100%;\n" +
                "    padding:15px 10px;\n" +
                "    box-sizing: border-box;\n" +
                "}";
        FileCorso css = new FileCss(PATH_CSS, "mystyle.css", contentCss);
        css.create();

        saveHtmlFile(corso);

        return corso;
    }



    public void saveHtmlFile(ComponenteCorso componenteCorso) throws IOException {

        String contentHtml = "<!DOCTYPE html>\n" +
                "<html lang=\"it-IT\">\n" +
                "\t<head>\n" +
                "\t\t<meta charset=\"UTF-8\">\n" +
                "\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/mystyle.css\">\n" +
                "\t\t<style type=\"text/css\"></style>\n" +
                "\t\t<title>" + componenteCorso.getTitolo() + "</title>" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\t" + componenteCorso.getOuterHtml() + "\n" +
                "\t</body>\n" +
                "</html>";
        componenteCorso.setNome(componenteCorso.getNome().replaceAll("/|\\\\", "-").replaceAll(":", " -"));
        componenteCorso.setContent(contentHtml);
        componenteCorso.create();

    }

}
