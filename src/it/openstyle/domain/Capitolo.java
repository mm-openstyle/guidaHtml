package it.openstyle.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manuel on 07/04/2017.
 */
public class Capitolo extends ComponenteCorso {

    private List<Allegato> allegati = new ArrayList<>();
    private List<Immagine> immagini = new ArrayList<>();

    public Capitolo(String path, String nome, String content) {
        super(path, nome, content);
    }

    public List<Allegato> getAllegati() {
        return allegati;
    }

    public void setAllegati(List<Allegato> allegati) {
        this.allegati = allegati;
    }

    public List<Immagine> getImmagini() {
        return immagini;
    }

    public void setImmagini(List<Immagine> immagini) {
        this.immagini = immagini;
    }

    public void addImmagine(Immagine immagine) {
        immagini.add(immagine);
    }

    public void addAllegato(Allegato allegato) {
        allegati.add(allegato);
    }
}
