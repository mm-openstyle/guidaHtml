package it.openstyle.domain;

import org.jsoup.Jsoup;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Manuel on 07/04/2017.
 */
public class Allegato extends FileCorso {

    public Allegato(String path, String nome, String content) {
        super(path, nome, content);
    }

    @Override
    public void create() throws IOException {

        File dir = new File(this.getPath());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir.getPath(), this.getNome());
        if (!file.exists()) {
            try {
                byte[] bytes;
                try {
                    String url = this.getContent();
                    if (!url.contains("http://www.html.it/")) {
                        url = "http://www.html.it" + url;
                    }
                    bytes = Jsoup.connect(url).ignoreContentType(true).execute().bodyAsBytes();
                } catch (IllegalArgumentException ex) {
                    bytes = Jsoup.connect("http://www.html.it/wp-content/uploads/guide/zip/" + this.getNome()).ignoreContentType(true).execute().bodyAsBytes();
                }
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file.getPath());
                fos.write(bytes);
                fos.close();
            }
            catch (IOException e) {
                System.out.println("Exception in FileCorso " + file.getAbsolutePath() + ": " + e.getMessage());
            }
        }

    }
}
