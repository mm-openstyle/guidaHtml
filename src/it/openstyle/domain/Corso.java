package it.openstyle.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manuel on 07/04/2017.
 */
public class Corso extends ComponenteCorso {

    private List<Capitolo> capitoli = new ArrayList<>();

    public Corso(String path, String nome, String content) {
        super(path, nome, content);
    }

    public List<Capitolo> getCapitoli() {
        return capitoli;
    }

    public void setCapitoli(List<Capitolo> capitoli) {
        this.capitoli = capitoli;
    }

    public void addCapitolo(Capitolo capitolo) {
        capitoli.add(capitolo);
    }

    @Override
    public String getOuterHtml() {
        String outerHtml = "";
        for (Capitolo capitolo : capitoli) {
            if (!outerHtml.isEmpty())
                outerHtml += "\n ";
            outerHtml += capitolo.getOuterHtml();
        }

        return outerHtml;
    }

    @Override
    public String toString() {
        int capitoli = 0, immagini = 0, allegati = 0;

        capitoli = this.getCapitoli().size();
        for (Capitolo capitolo : this.getCapitoli()) {
            immagini += capitolo.getImmagini().size();
            allegati += capitolo.getAllegati().size();
        }

        return "Corso: " + this.getTitolo() + "\n"
                + "Capitoli: " + capitoli + "\n"
                + "Immagini: " + immagini + "\n"
                + "Allegati: " + allegati + "\n";
    }
}
