package it.openstyle.domain;

import org.jsoup.Jsoup;

import java.io.*;

/**
 * Created by Manuel on 07/04/2017.
 */
public class Immagine extends FileCorso {

    public Immagine (String path, String nome, String content) {
        super(path, nome, content);
    }

    @Override
    public void create() throws IOException {

        File dir = new File(this.getPath());
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir.getPath(), this.getNome());
        if (!file.exists()) {
            try {
                byte[] bytes = null;
                try {
                    bytes = Jsoup.connect(this.getContent()).ignoreContentType(true).execute().bodyAsBytes();
                } catch (IllegalArgumentException ex) {
                    bytes = Jsoup.connect("http://www.html.it/wp-content/uploads/2016/12/" + this.getNome()).ignoreContentType(true).execute().bodyAsBytes();
                } finally {
                    file.createNewFile();
                    FileOutputStream fos = new FileOutputStream(file.getPath());
                    fos.write(bytes);
                    fos.close();
                }
            } catch (IOException e) {
                System.out.println("Exception in FileCorso " + file.getAbsolutePath() + ": " + e.getMessage());
            }
        }

    }
}
