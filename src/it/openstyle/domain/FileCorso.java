package it.openstyle.domain;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Manuel on 10/04/2017.
 */
public abstract class FileCorso {

    private String nome;
    private String path;
    private String content;

    public FileCorso (String path, String nome, String content) {
        this.nome = nome;
        this.path = path;
        this.content = content;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAbsolutePath() {
        return new File(path, nome).getAbsolutePath();
    }

    public void create() throws IOException {

        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();

        File file = new File(dir.getPath(), nome);
        try {
            System.out.println(file.getAbsolutePath());
            if (!file.exists()) {
                file.createNewFile();
                FileWriter fileWriter = new FileWriter(file.getPath());
                BufferedWriter writer = new BufferedWriter(fileWriter);
                writer.write(content);
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Exception in FileCorso " + file.getAbsolutePath() + ": " + e.getMessage());
        }
    }
}
