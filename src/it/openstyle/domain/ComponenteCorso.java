package it.openstyle.domain;

/**
 * Created by Manuel on 10/04/2017.
 */
public abstract class ComponenteCorso extends FileCorso {

    private String titolo;
    private String outerHtml;

    public ComponenteCorso(String path, String nome, String content) {
        super(path, nome + ".html", content);
        this.titolo = nome;
    }

    public String getTitolo() {
        return titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getOuterHtml() {
        return outerHtml;
    }

    public void setOuterHtml(String outerHtml) {
        this.outerHtml = outerHtml;
    }

}
